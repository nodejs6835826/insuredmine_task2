// combinedServer.js
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const os = require('os-utils');
const cmd = require('node-cmd');

const app = express();
const port = 30000;

// Middleware
app.use(bodyParser.json());

// MongoDB connection
mongoose.connect('mongodb://localhost:27017/scheduledMessages', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const messageSchema = new mongoose.Schema({
  message: String,
  day: String,
  time: String,
});

const Message = mongoose.model('Message', messageSchema);

app.post('/schedule-message', async (req, res) => {
  const { message, day, time } = req.body;

  if (!message || !day || !time) {
    return res.status(400).send('Missing message, day, or time');
  }

  try {
    const newMessage = new Message({ message, day, time });
    await newMessage.save();
    res.status(201).send('Message scheduled successfully');
  } catch (error) {
    res.status(500).send('Internal server error');
  }
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});

// Monitor CPU usage and restart server if necessary
function checkCpuUsage() {
  os.cpuUsage(function(v){
    console.log('CPU Usage (%): ' + v * 100);
    if (v > 0.7) { // 70% usage
      console.log('CPU Usage is above 70%, restarting server...');
      restartServer();
    }
  });
}

function restartServer() {
  // Command to restart node server
  cmd.run('pm2 restart all');
}

// Check CPU usage every minute
setInterval(checkCpuUsage, 60000);
